<?php
	include("connect.php");
	include("fpdf/fpdf.php");

	$con = mysqli_connect($host, $usr, $pw, $db);

	$query = mysqli_query($con, "SELECT * FROM Factura WHERE Fac_Num_Unico = $_GET[Fac_Num_Unico]");
	$result = mysqli_fetch_array($query, MYSQLI_ASSOC);

	$pdf = new FPDF();
	$pdf->AddPage();
	$pdf->SetFont("Arial", "B", 16);
	$pdf->setTitle("Factura");
	$pdf->SetMargins(20, 25, 20);
	$pdf->SetX(55);
	$pdf->Cell(100, 10, "Factura De Venta" , 1, 1, 'C');
	$pdf->SetX(55);
	$pdf->Cell(100, 10, "Numero: ".$_GET['Fac_Num_Unico'], 1, 1, 'C');
	$pdf->Ln(10);
	$date = date_create($result['Fac_Fecha']);
	$pdf->Cell(0, 10, "Fecha: ".date_format($date, 'd-m-Y'), 1, 1);

	$var_query = mysqli_query($con, "SELECT Cli_Nombre FROM Clientes WHERE Cli_Rut = '$result[Cli_Rut]'");
	$var_result = mysqli_fetch_array($var_query, MYSQLI_ASSOC);

	$pdf->Cell(0, 10, "Cliente: ".$var_result['Cli_Nombre'], 1, 1);
	$pdf->Cell(0, 10, "Rut: ".$result['Cli_Rut'], 1, 1);

	$var_query = mysqli_query($con, "SELECT Direccion FROM Despacho WHERE Cod_Despacho = '$result[Cod_Despacho]'");
	$var_result = mysqli_fetch_array($var_query, MYSQLI_ASSOC);

	$pdf->Cell(0, 10, "Direccion: ".$var_result['Direccion'], 1, 1);

	$var_query = mysqli_query($con, "SELECT R.Nombre_Region FROM Despacho D, Direccion DD, Region R WHERE D.Direccion = '$var_result[Direccion]' AND D.Direccion = DD.Direccion AND DD.Numero_Region = R.Numero_Region");
	$var_result = mysqli_fetch_array($var_query, MYSQLI_ASSOC);

	$pdf->Cell(0, 10, "Region: ".$var_result['Nombre_Region'], 1, 1);

	$pdf->Cell(0, 10, "Pedido: ".$result['pcodigo'], 1, 1);
	$var_query_mp = mysqli_query($con, "SELECT MP_Descripcion FROM Modo_de_Pagos WHERE MP_Codigo = '$result[MP_Codigo]'");
	$var_result_mp = mysqli_fetch_array($var_query_mp, MYSQLI_ASSOC);
	$pdf->Cell(0, 10, "Medio de Pago: ".$var_result_mp['MP_Descripcion'], 1, 1);
	$pdf->Ln(20);

	$width = array(30, 20, 60, 30, 30);
	$pdf->Cell($width[0], 10, "Articulo", 1, 0);
	$pdf->Cell($width[1], 10, "Cant", 1, 0);
	$pdf->Cell($width[2], 10, "Descripcion", 1, 0);
	$pdf->Cell($width[3], 10, "Unitario", 1, 0);
	$pdf->Cell($width[4], 10, "Total", 1, 0);
	$pdf->Ln();

	$var_query = mysqli_query($con, "SELECT art_codigo, COUNT(art_codigo) AS Cantidad, Art_Descripcion, Art_Precio, SUM(Art_Precio) AS Total FROM Pedidos_Articulos NATURAL JOIN Articulos WHERE pcodigo = '$result[pcodigo]' GROUP BY art_codigo");

	while($var_result = mysqli_fetch_array($var_query, MYSQLI_ASSOC)){
		$pdf->Cell($width[0], 10, $var_result['art_codigo'], 1, 0);
		$pdf->Cell($width[1], 10, $var_result['Cantidad'], 1, 0);
		$pdf->Cell($width[2], 10, $var_result['Art_Descripcion'], 1, 0);
		$pdf->Cell($width[3], 10, $var_result['Art_Precio'], 1, 0);
		$pdf->Cell($width[4], 10, $var_result['Total'], 1, 0);
		$pdf->Ln();
	}
	$pdf->Cell(array_sum($width)-$width[4], 10, "Monto Neto: ", 1, 0, 'R');
	$pdf->Cell($width[4], 10, $result['Neto'], 1, 1);
	$pdf->Cell(array_sum($width)-$width[4], 10, "IVA 19%: ", 1, 0, 'R');
	$pdf->Cell($width[4], 10, ($result['Neto']*0.19), 1, 1);
	$pdf->Cell(array_sum($width)-$width[4], 10, "Monto Bruto: ", 1, 0, 'R');
	$pdf->Cell($width[4], 10, $result['Bruto'], 1, 1);
	$pdf->output('factura_'.$result['Fac_Num_Unico'].'.pdf', 'I');

	mysqli_close($con);
?>