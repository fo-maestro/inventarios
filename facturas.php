<?php
	session_start();
	if(!isset($_SESSION['username'])){
		header("Location: restricted.php");
	}
?>

<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
		<title>Inventarios</title>
		<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
		<link rel="stylesheet" type="text/css" href="font-awesome/css/font-awesome.min.css">
		<style type="text/css">
			body {
				padding-top:51px;
			}

			
		</style>
		<link rel="stylesheet" type="text/css" href="css/sidebar.css">
	</head>
	<body>
	<div id="create-modal" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<form action="facturas.php" id="insert-form">
					<div class="modal-header">
						<button type="button" data-dismiss="modal" class="close"><span>&times;</span></button>
						<h4 class="modal-title text-center">Insertar Factura</h4>
					</div>
					<div class="modal-body">
						<div class="form-group">
							<input type="text" class="form-control" placeholder="Numero" name="numero">
						</div>
						<div class="form-group">
							<input type="date" placeholder="Fecha" class="form-control" name="fecha">
						</div>
						<div class="form-group">
							<select name="rut_cli" id="client-select" class="form-control">
								<?php
									include("connect.php");

									$con = mysqli_connect($host, $usr, $pw, $db);
									mysqli_query($con, "SET NAMES 'utf8'");
									mysqli_query($con, "SET CHARACTER_SET 'utf8'");
									$query = mysqli_query($con, "SELECT * FROM Clientes");
									echo '<option selected disabled>Seleccione un Cliente</option>';
									while($result = mysqli_fetch_array($query, MYSQLI_ASSOC)){
										echo '<option value="'.$result['Cli_Rut'].'">'.$result['Cli_Rut'].'</option>';
									}
									mysqli_close($con);
								?>
							</select>
						</div>
						<div class="form-group">
							<select name="codigo" class="form-control">
								<?php
									include("connect.php");

									$con = mysqli_connect($host, $usr, $pw, $db);
									mysqli_query($con, "SET NAMES 'utf8'");
									mysqli_query($con, "SET CHARACTER_SET 'utf8'");
									$query = mysqli_query($con, "SELECT * FROM Despacho");
									echo '<option selected disabled>Seleccione Despacho</option>';
									while($result = mysqli_fetch_array($query, MYSQLI_ASSOC)){
										echo '<option value="'.$result['Cod_Despacho'].'">'.$result['Direccion'].'</option>';
									}
									mysqli_close($con);
								?>
							</select>
						</div>
						<div class="form-group">
							<select name="rut_ven" class="form-control">
								<?php
									include("connect.php");

									$con = mysqli_connect($host, $usr, $pw, $db);
									mysqli_query($con, "SET NAMES 'utf8'");
									mysqli_query($con, "SET CHARACTER_SET 'utf8'");
									$query = mysqli_query($con, "SELECT * FROM Vendedor");
									echo '<option selected disabled>Seleccione un Vendedor</option>';
									while($result = mysqli_fetch_array($query, MYSQLI_ASSOC)){
										echo '<option value="'.$result['Ven_Rut'].'">'.$result['Ven_Rut'].'</option>';
									}
									mysqli_close($con);
								?>
							</select>
						</div>
						<div class="form-group">
							<select name="pago" class="form-control">
								<?php
									include("connect.php");

									$con = mysqli_connect($host, $usr, $pw, $db);
									mysqli_query($con, "SET NAMES 'utf8'");
									mysqli_query($con, "SET CHARACTER_SET 'utf8'");
									$query = mysqli_query($con, "SELECT * FROM Modo_de_Pagos");
									echo '<option selected disabled>Seleccione un Medio de Pago</option>';
									while($result = mysqli_fetch_array($query, MYSQLI_ASSOC)){
										echo '<option value="'.$result['MP_Codigo'].'">'.$result['MP_Descripcion'].'</option>';
									}
									mysqli_close($con);
								?>
							</select>
						</div>
						<div class="form-group">
							<select name="pedido" id ="pedido-control" class="form-control">
								<option selected disabled>Seleccione un Pedido</option>
							</select>
						</div>
						<div class="form-group">
							<input type="text" placeholder="Monto Neto" name="neto" id="neto-create" class="form-control" readonly>
						</div>
						<div class="form-group">
							<input type="text" placeholder="Monto Bruto" name="bruto" id="bruto-create" class="form-control" readonly>
						</div>
					</div>
					<div class="modal-footer">
						<div class="row">
							<div class="col-md-8">
								<div class="has-error">
									<label for="" class="control-label pull-left sr-only" id="label-insert"></label>
								</div>
							</div>
							<div class="col-md-4">					
								<button type="button" data-dismiss="modal" class="btn btn-default">Cancelar</button>
								<button type="submit" id="insert-modal" class="btn btn-primary">Insertar</button>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div id="edit-modal" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<form action="facturas.php" id="edit-form">
					<div class="modal-header">
						<button type="button" data-dismiss="modal" class="close"><span>&times;</span></button>
						<h4 class="modal-title text-center" id="edit-tittle"></h4>
					</div>
					<div class="modal-body" id="body-form">
						<div class="form-group">
							<input type="text" class="form-control" placeholder="Numero" name="Fac_Num_Unico">
						</div>
						<div class="form-group">
							<input type="date" placeholder="Fecha" class="form-control" name="Fac_Fecha">
						</div>
						<div class="form-group">
							<select name="Cli_Rut" id="client-select-edit" class="form-control">
								<?php
									include("connect.php");

									$con = mysqli_connect($host, $usr, $pw, $db);
									mysqli_query($con, "SET NAMES 'utf8'");
									mysqli_query($con, "SET CHARACTER_SET 'utf8'");
									$query = mysqli_query($con, "SELECT * FROM Clientes");
									echo '<option selected disabled>Seleccione un Cliente</option>';
									while($result = mysqli_fetch_array($query, MYSQLI_ASSOC)){
										echo '<option value="'.$result['Cli_Rut'].'">'.$result['Cli_Rut'].'</option>';
									}
									mysqli_close($con);
								?>
							</select>
						</div>
						<div class="form-group">
							<select name="Cod_Despacho" class="form-control">
								<?php
									include("connect.php");

									$con = mysqli_connect($host, $usr, $pw, $db);
									mysqli_query($con, "SET NAMES 'utf8'");
									mysqli_query($con, "SET CHARACTER_SET 'utf8'");
									$query = mysqli_query($con, "SELECT * FROM Despacho");
									echo '<option selected disabled>Seleccione Despacho</option>';
									while($result = mysqli_fetch_array($query, MYSQLI_ASSOC)){
										echo '<option value="'.$result['Cod_Despacho'].'">'.$result['Direccion'].'</option>';
									}
									mysqli_close($con);
								?>
							</select>
						</div>
						<div class="form-group">
							<select name="Ven_Rut" class="form-control">
								<?php
									include("connect.php");

									$con = mysqli_connect($host, $usr, $pw, $db);
									mysqli_query($con, "SET NAMES 'utf8'");
									mysqli_query($con, "SET CHARACTER_SET 'utf8'");
									$query = mysqli_query($con, "SELECT * FROM Vendedor");
									echo '<option selected disabled>Seleccione un Vendedor</option>';
									while($result = mysqli_fetch_array($query, MYSQLI_ASSOC)){
										echo '<option value="'.$result['Ven_Rut'].'">'.$result['Ven_Rut'].'</option>';
									}
									mysqli_close($con);
								?>
							</select>
						</div>
						<div class="form-group">
							<select name="MP_Codigo" class="form-control">
								<?php
									include("connect.php");

									$con = mysqli_connect($host, $usr, $pw, $db);
									mysqli_query($con, "SET NAMES 'utf8'");
									mysqli_query($con, "SET CHARACTER_SET 'utf8'");
									$query = mysqli_query($con, "SELECT * FROM Modo_de_Pagos");
									echo '<option selected disabled>Seleccione un Medio de Pago</option>';
									while($result = mysqli_fetch_array($query, MYSQLI_ASSOC)){
										echo '<option value="'.$result['MP_Codigo'].'">'.$result['MP_Descripcion'].'</option>';
									}
									mysqli_close($con);
								?>
							</select>
						</div>
						<div class="form-group">
							<select name="pcodigo" id ="pedido-control-edit" class="form-control">
								<option selected disabled>Seleccione un Pedido</option>
							</select>
						</div>
						<div class="form-group">
							<input type="text" placeholder="Monto Neto" name="Neto" id="neto-edit" class="form-control" readonly>
						</div>
						<div class="form-group">
							<input type="text" placeholder="Monto Bruto" name="Bruto" id="bruto-edit" class="form-control" readonly>
						</div>
					</div>
					<div class="modal-footer">
						<div class="row">
							<div class="col-md-8">
								<div class="has-error">
									<label for="" class="control-label pull-left sr-only" id="label-edit"></label>
								</div>
							</div>
							<div class="col-md-4">					
								<button type="button" data-dismiss="modal" class="btn btn-default">Cancelar</button>
								<button type="submit" id="edit-modal-btn" class="btn btn-success">Editar</button>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div id="delete-modal" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<form action="facturas.php" id="delete-form">
					<div class="modal-header">
						<button type="button" data-dismiss="modal" class="close"><span>&times;</span></button>
						<h4 class="modal-title text-center" id="delete-title"></h4>
					</div>
					<div class="modal-body">
						<p id="delete-text"></p>
					</div>
					<div class="modal-footer">
						<button type="button" data-dismiss="modal" class="btn btn-default">Cancelar</button>
						<button type="submit" id="delete-modal-btn" class="btn btn-danger">Eliminar</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<label class="sr-only" id="table-name">Factura</label>
	<label class="sr-only" id="filter-tab">Fac_Num_Unico</label>
		<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" data-toggle="collapse" data-target=".collapsable" class="navbar-toggle">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a href="#" id="menu-toggle" class="navbar-brand">Inventarios</a>
				</div>
				<div class="collapse navbar-collapse collapsable">
					<ul class="nav navbar-nav navbar-right">
						<li><a href="" onclick="prevent(event)" id="date-time"><?php echo $_SESSION['date-conection'];?></a></li>
						<li><a href="login.php" id="log-out">Log Out</a></li>
					</ul>
				</div>
			</div>
		</nav>

		<div id="wrapper">
			<div id="sidebar-wrapper">
				<ul class="sidebar-nav">
					<li><a href="index.php"><i class="glyphicon glyphicon-home"></i> Inicio</a></li>
					<li><a href="#" data-toggle="collapse" data-target="#submenu1"><i class="glyphicon glyphicon-user"></i> Clientes</a></li>
						<ul id="submenu1" role="menu" class="collapse sidebar-nav-submenu">
							<li><a href="clients.php">Clientes</a></li>
							<li><a href="cart_client.php">Cartera de Clientes</a></li>
						</ul>
					<li><a href="vendedores.php"><i class="glyphicon glyphicon-user"></i> Vendedores</a></li>
					<li><a href="bodegas.php"><i class="fa fa-truck"></i> Bodegas</a></li>
					<li><a href="productos.php"><i class="glyphicon glyphicon-shopping-cart"></i> Articulos</a></li>
					<li class="active-bar"><a href=""><i class="fa fa-credit-card"></i> Facturacion</a></li>
					<li><a href="#" data-toggle="collapse" data-target="#submenu2"><i class="glyphicon glyphicon-usd"></i> Ventas</a></li>
						<ul id="submenu2" role="menu" class="collapse sidebar-nav-submenu">
							<li><a href="vent_vendedor.php">Ventas Vendedor</a></li>
							<li><a href="vent_articulos.php">Ventas Articulos de Bodega</a></li>
						</ul>
					<li><a href="#" data-toggle="collapse" data-target="#submenu3"><i class="glyphicon glyphicon-tags"></i> Consultas</a></li>
					<ul id="submenu3" role="menu" class="collapse sidebar-nav-submenu">
						<li><a href="vent_bodegas.php">Top Ventas de Bodega</a></li>
						<li><a href="inactivos.php">Clientes Inactivos</a></li>
						<li><a href="vendedores_mp.php">Vendedores Modos de Pago</a></li>
						<li><a href="filtro_articulos.php">Filtro Articulos</a></li>
					</ul>
				</ul>
			</div>

			<div class="page-content-wrapper">
				<div class="container-fluid">
					<div class="row">
						<div class="col-lg-12">
							<div class="page-header text-center">
								<h1>Facturacion</h1>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-6">
									<p><button type="button" class="btn btn-primary" id="insert">Insertar</button></p>
								</div>
								<div class="col-md-offset-1 col-md-5">
									<div class="input-group">
										<span class="input-group-addon"><i class=" fa fa-search"></i></span>
										<input type="text" class="form-control" id="find-control-fact">
										<span class="input-group-btn">
											<select type="button" class="btn btn-default" id="filter">
												<option value="Fac_Num_Unico">Numero</option>
												<option value="Fac_Fecha">Fecha</option>
												<option value="Cli_Rut">Rut Cliente</option>
												<option value="Cod_Despacho">Despacho</option>
												<option value="Ven_Rut">Rut Vendedor</option>
												<option value="MP_Codigo">Medio Pago</option>
												<option value="pcodigo">Pedido</option>
												<option value="Neto">Neto</option>
												<option value="Bruto">Bruto</option>
											</select>
										</span>
									</div>
								</div>
							</div>
							<table class="table table-bordered">
								<thead>
									<tr>
										<th>Numero</th>
										<th>Fecha</th>
										<th>Rut Cliente</th>
										<th>Codigo Despacho</th>
										<th>Rut Vendedor</th>
										<th>Medio de Pago</th>
										<th>Pedido</th>
										<th>Neto</th>
										<th>Bruto</th>
										<th>Opcion</th>
									</tr>
								</thead>
								<tbody id="table-content">
									<?php
										include("connect.php");

										$con = mysqli_connect($host, $usr, $pw, $db);
										mysqli_query($con, "SET NAMES 'utf8'");
										mysqli_query($con, "SET CHARACTER_SET 'utf8'");
										$query = mysqli_query($con, "SELECT * FROM Factura");
										while($row = mysqli_fetch_array($query, MYSQLI_ASSOC)){
											echo "<tr>";
											foreach($row as $key => $value){
												echo "<td>".$value."</td>";
											}
											echo '<td><button type="button" onclick="editModalFac(this)" class="btn btn-success">Editar</button> ';
											echo '<button type="button" onclick="deleteModal(this)" class="btn btn-danger">Eliminar</button> ';
											echo '<a href="detalle.php?Fac_Num_Unico='.$row['Fac_Num_Unico'].'" class="btn btn-default">Detalle</a></td>';
											echo "</tr>";
										}
										mysqli_close($con);
									?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>

		<script src="js/jquery.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/ajaxModal.js"></script>
	</body>
</html>