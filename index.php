<?php
	session_start();
	if(!isset($_SESSION['username'])){
		header("Location: restricted.php");
	}
?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Inventarios</title>
		<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
		<link rel="stylesheet" type="text/css" href="font-awesome/css/font-awesome.min.css">
		<style type="text/css">
			body {
				padding-top:51px;
			}

			
		</style>
		<link rel="stylesheet" type="text/css" href="css/sidebar.css">
	</head>
	<body>
		<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" data-toggle="collapse" data-target=".collapsable" class="navbar-toggle">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a href="#" id="menu-toggle" class="navbar-brand">Inventarios</a>
				</div>
				<div class="collapse navbar-collapse collapsable">
					<ul class="nav navbar-nav navbar-right">
						<li><a href="" onclick="prevent(event)" id="date-time"><?php echo $_SESSION['date-conection'];?></a></li>
						<li><a href="login.php" id="log-out">Log Out</a></li>
					</ul>
				</div>
			</div>
		</nav>

		<div id="wrapper">
			<div id="sidebar-wrapper">
				<ul class="sidebar-nav">
					<li class="active-bar"><a href=""><i class="glyphicon glyphicon-home"></i> Inicio</a></li>
					<li><a href="#" data-toggle="collapse" data-target="#submenu1"><i class="glyphicon glyphicon-user"></i> Clientes</a></li>
						<ul id="submenu1" role="menu" class="collapse sidebar-nav-submenu">
							<li><a href="clients.php">Clientes</a></li>
							<li><a href="cart_client.php">Cartera de Clientes</a></li>
						</ul>
					<li><a href="vendedores.php"><i class="glyphicon glyphicon-user"></i> Vendedores</a></li>
					<li><a href="bodegas.php"><i class="fa fa-truck	"></i> Bodegas</a></li>
					<li><a href="productos.php"><i class="glyphicon glyphicon-shopping-cart"></i> Articulos</a></li>
					<li><a href="facturas.php"><i class="fa fa-credit-card"></i> Facturacion</a></li>
					<li><a href="#" data-toggle="collapse" data-target="#submenu2"><i class="glyphicon glyphicon-usd"></i> Ventas</a></li>
						<ul id="submenu2" role="menu" class="collapse sidebar-nav-submenu">
							<li><a href="vent_vendedor.php">Ventas Vendedor</a></li>
							<li><a href="vent_articulos.php">Ventas Articulos de Bodega</a></li>
						</ul>
					<li><a href="#" data-toggle="collapse" data-target="#submenu3"><i class="glyphicon glyphicon-tags"></i> Consultas</a></li>
					<ul id="submenu3" role="menu" class="collapse sidebar-nav-submenu">
						<li><a href="vent_bodegas.php">Top Ventas de Bodega</a></li>
						<li><a href="inactivos.php">Clientes Inactivos</a></li>
						<li><a href="vendedores_mp.php">Vendedores Modos de Pago</a></li>
						<li><a href="filtro_articulos.php">Filtro Articulos</a></li>
					</ul>
				</ul>
			</div>

			<div class="page-content-wrapper">
				<div class="container-fluid">
					<div class="row">
						<div class="col-lg-12">
							<div class="page-header text-center">
								<h1>¡Bienvenido!</h1>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-offset-3 col-md-9">
							<img src="Admin.png" alt="">
						</div>
					</div>
				</div>
			</div>
		</div>

		<script src="js/jquery.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script>
		    $("#menu-toggle").click(function(e) {
		        e.preventDefault();
		        $("#wrapper").toggleClass("toggled");
		    });

		    $("#log-out").click(function(e){
		    	$.ajax({
		    		type: 'POST',
		    		url: 'close_session.php'
		    	});
		    });

		</script>
	</body>
</html>