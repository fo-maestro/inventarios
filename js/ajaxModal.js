$("#menu-toggle").click(function(e) {
	e.preventDefault();
	$("#wrapper").toggleClass("toggled");
});

$("#log-out").click(function(e){
	e.preventDefault();
	$.ajax({
		type: 'POST',
		url: 'close_session.php',
		success: function(response){
		 	if(response == "ok"){
		  		window.location = "login.php";
		 	}
		}
	});
});

$("#find-control").keyup(function(e){
	$.ajax({
		type: 'POST',
		url: 'filter_db.php',
		data: { field: $("#filter").find(":selected").val(), condition: $(this).val(), tablename: $("#table-name").text() },
		success: function(response){
		 	document.getElementById("table-content").innerHTML = response;
		}
	});
});

$("#find-control-fact").keyup(function(e){
	$.ajax({
		type: 'POST',
		url: 'filter_db_fact.php',
		data: { field: $("#filter").find(":selected").val(), condition: $(this).val(), tablename: $("#table-name").text() },
		success: function(response){
		 	document.getElementById("table-content").innerHTML = response;
		}
	});
});

$("#find-control-cart-client").keyup(function(e){
	$.ajax({
		type: 'POST',
		url: 'filter_db_cart_client.php',
		data: { field: $("#filter").find(":selected").val(), condition: $(this).val(), tablename: $("#table-name").text() },
		success: function(response){
		 	document.getElementById("table-content").innerHTML = response;
		}
	});
});

var data;

function editModal(object){
	var child = object.parentElement.parentElement.children;
	document.getElementById("edit-tittle").innerHTML = "Editar "+document.getElementById("table-name").innerHTML+": "+child[0].innerHTML;
	data = child[0].innerHTML;

	var valueModal = document.getElementById("body-form");
	for(var i = 0; i < valueModal.children.length; i++){
		if(valueModal.children[i].children[0].hasChildNodes()){
			for(var j = 0; j < valueModal.children[i].children[0].children.length; j++){
				if(valueModal.children[i].children[0].children[j].value == child[i].innerHTML){
					valueModal.children[i].children[0].children[j].selected = true;
				}
			}
		}else{
			valueModal.children[i].children[0].value = child[i].innerHTML;
		}
	}
	$("#edit-modal").modal("show");		    	
}

function deleteModal(object){
	var child = object.parentElement.parentElement.children;
	document.getElementById("delete-title").innerHTML = "Eliminar "+document.getElementById("table-name").innerHTML;
	data = child[0].innerHTML;
	document.getElementById("delete-text").innerHTML = "Esta seguro que desea eliminar "+document.getElementById("table-name").innerHTML+": "+data;
	$("#delete-modal").modal("show");
}

$("#insert").click(function(e){
	$("#create-modal").modal("show");
});

$("#insert-modal").click(function(e){
	e.preventDefault();
	$.ajax({
		type: 'POST',
		url: 'insert.php',
		data: "tablename="+$("#table-name").text()+"&"+$("#insert-form").serialize(),
		success: function(response){
		 	if(response == "ok"){
		  		document.getElementById("label-insert").className = "control-label pull-left sr-only";
		  		document.getElementById("insert-form").submit();
		 	}else{
		  		var component = document.getElementById("label-insert");
		  		component.className = "control-label pull-left";
		  		component.innerHTML = response;
		 	}
		}
	});
});

$("#edit-modal-btn").click(function(e){
	e.preventDefault();
	$.ajax({
		type: 'POST',
		url: 'update.php',
		data: "tablename="+$("#table-name").text()+"&filter="+data+"&"+$("#edit-form").serialize(),
		success: function(response){
		 	if(response == "ok"){
		  		document.getElementById("label-edit").className = "control-label pull-left sr-only";
		  		document.getElementById("edit-form").submit();
		 	}else{
		  		var component = document.getElementById("label-edit");
		  		component.className = "control-label pull-left";
		  		component.innerHTML = response;
		 	}
		}
	});
});

$("#delete-modal-btn").click(function(e){
	e.preventDefault();
	$.ajax({
		type: 'POST',
		url: 'delete.php',
		data: { tablename: $("#table-name").text(), 
		    filtername: document.getElementById("filter-tab").innerHTML,
		    filter: data },
		success: function(response){
		 	if(response == "ok"){
		  		document.getElementById("delete-form").submit();
		 	}
		}
	});
});

$("#client-select").change(function(object){
	$.ajax({
		type: 'POST',
		url: 'filter_pedidos.php',
		data: { Cli_rut: $("#client-select").val() },
		success: function(response){
			document.getElementById("neto-create").value = "";
			document.getElementById("bruto-create").value = "";
			document.getElementById("pedido-control").innerHTML = response;
		}
	});
});

function editModalFac(object){
	var child = object.parentElement.parentElement.children;
	$.ajax({
		type: 'POST',
		url: 'filter_pedidos.php',
		data: { Cli_rut: child[2].innerHTML },
		success: function(response){
			document.getElementById("pedido-control-edit").innerHTML = response;
			document.getElementById("edit-tittle").innerHTML = "Editar "+document.getElementById("table-name").innerHTML+": "+child[0].innerHTML;
			data = child[0].innerHTML;

			var valueModal = document.getElementById("body-form");
			for(var i = 0; i < valueModal.children.length; i++){
				if(valueModal.children[i].children[0].hasChildNodes()){
					for(var j = 0; j < valueModal.children[i].children[0].children.length; j++){
						if(valueModal.children[i].children[0].children[j].value == child[i].innerHTML){
							valueModal.children[i].children[0].children[j].selected = true;
						}
					}
				}else{
					valueModal.children[i].children[0].value = child[i].innerHTML;
				}
			}
			$("#edit-modal").modal("show");
		}
	});
}

$("#client-select-edit").change(function(object){
	$.ajax({
		type: 'POST',
		url: 'filter_pedidos.php',
		data: { Cli_rut: $("#client-select-edit").val() },
		success: function(response){
			document.getElementById("neto-edit").value = "";
			document.getElementById("bruto-edit").value = "";
			document.getElementById("pedido-control-edit").innerHTML = response;
		}
	});
});

$("#pedido-control").change(function(object){
	$.ajax({
		type: 'POST',
		url: 'get_monto.php',
		data: { pcodigo: $("#pedido-control").val() },
		success: function(response){
			document.getElementById("neto-create").value = response;
			document.getElementById("bruto-create").value = Math.round(response*1.19);
		}
	});
});

$("#pedido-control-edit").change(function(object){
	$.ajax({
		type: 'POST',
		url: 'get_monto.php',
		data: { pcodigo: $("#pedido-control-edit").val() },
		success: function(response){
			document.getElementById("neto-edit").value = response;
			document.getElementById("bruto-edit").value = Math.round(response*1.19);
		}
	});
});