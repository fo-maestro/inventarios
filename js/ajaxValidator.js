$(document).ready(function(){
	$("#btn-submit").click(function(e){

		if(validate(e)){
			ajax();
		}
	});

	$("#input-user").keypress(function(e){
		if(e.which == 13){
			if(validate(e)){
				ajax();
			}
		}else{
			setInput(document.getElementById("input-user"));
		}
	});

	$("#input-pass").keypress(function(e){
		if(e.which == 13){
			if(validate(e)){
				ajax();
			}
		}else{
			setInput(document.getElementById("input-pass"));
		}
	});
});

function ajax(){
	var data = $("#login-form").serialize();

	$.ajax({
		type: 'POST',
		url: 'validate.php',
		data: data+"&date="+getDate(),
		success: function(response){
			if(response == "incorrect-user"){
				var containerUser = document.getElementById("container-user");
				var labelUser = document.getElementById("label-user");
				var inputUser = document.getElementById("input-user");

				containerUser.className += " has-error";
				labelUser.className = "control-label";
				labelUser.innerHTML = "El usuario ingresado no es valido";
			}else if(response == "incorrect-pass"){
				var containerPass = document.getElementById("container-pass");
				var labelPass = document.getElementById("label-pass");
				var inputPass = document.getElementById("input-pass");

				containerPass.className += " has-error";
				labelPass.className = "control-label";
				labelPass.innerHTML = "La contraseña ingresada no es valida";
				inputPass.value = "";
			}else if(response == "ok"){
				document.getElementById("login-form").submit();
			}
		}
	});
	return false;
}

function getDate(){
	var date = new Date();
	return "Ultima Conexión: "+date.getDate()+"/"+(date.getMonth()+1)+"/"+date.getFullYear()+" "+date.getHours()+":"+date.getMinutes();
}