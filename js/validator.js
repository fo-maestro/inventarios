function validate(event){
	var containerUser = document.getElementById("container-user");
	var labelUser = document.getElementById("label-user");
	var inputUser = document.getElementById("input-user");
	var containerPass = document.getElementById("container-pass");
	var labelPass = document.getElementById("label-pass");
	var inputPass = document.getElementById("input-pass");
	
	if(inputUser.value.length == 0 && inputPass.value.length == 0){
		containerUser.className += " has-error";
		labelUser.className = "control-label";
		labelUser.innerHTML = "Debe llenar el campo de usuario";

		containerPass.className += " has-error";
		labelPass.className = "control-label";
		labelPass.innerHTML = "Debe llenar el campo de contraseña";
		event.preventDefault(); 

		return false;
	}else if(inputUser.value.length == 0){
		containerUser.className += " has-error";
		labelUser.className = "control-label";
		labelUser.innerHTML = "Debe llenar el campo de usuario";

		event.preventDefault();

		return false;
	}else if(inputPass.value.length == 0){
		containerPass.className += " has-error";
		labelPass.className = "control-label";
		labelPass.innerHTML = "Debe llenar el campo de contraseña";
		event.preventDefault(); 

		return false;
	}

	event.preventDefault();
	
	return true;
}

function setInput(element){
	var parent = element.parentNode;
	if(element.id == "input-user"){
		parent.className = "form-group";
		var labelUser = document.getElementById("label-user");
		labelUser.className = "sr-only";
	}else if(element.id == "input-pass"){
		parent.className = "form-group";
		var labelPass = document.getElementById("label-pass");
		labelPass.className = "sr-only";
	}
}