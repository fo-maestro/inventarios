<?php
	session_start();
	if(isset($_SESSION['username'])){
		header("Location: index.php");
	}
?>

<!DOCTYPE html>
	<html>
	<head>
		<meta charset="UTF-8">
		<title>Log In</title>
		<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
		<style type="text/css">
			body {
			    padding-top: 90px;
			    background-color: rgb(0, 71, 128);
			}
			.panel-login {
				border-color: #ccc;
				-webkit-box-shadow: 0px 2px 3px 0px rgba(0,0,0,0.2);
				-moz-box-shadow: 0px 2px 3px 0px rgba(0,0,0,0.2);
				box-shadow: 0px 2px 3px 0px rgba(0,0,0,0.2);
			}
		</style>
	</head>
	<body>
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-md-offset-3">
					<div class="panel panel-login">
					<div class="page-header text-center">
						<h2>Log In</h2>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-lg-12">
								<form action="index.php" method="post" role="form" id="login-form">
									<div class="form-group" id="container-user">
										<input type="text" id="input-user" onblur="setInput(this)" placeholder="Usuario..." class="form-control" name="user">
										<label id="label-user" class="sr-only"></label>
									</div>
									<div class="form-group" id="container-pass">
										<input type="password" id="input-pass" onblur="setInput(this)" placeholder="Contraseña..." class="form-control" name="pass">
										<label id="label-pass" class="sr-only"></label>
									</div>
									<div class="form-group text-center">
										<button type="submit" class="btn btn-info btn-lg btn-block" name="submit-button" id="btn-submit" >Ingresar</button>
									</div>
								</form>
							</div>
						</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<script src="js/validator.js"></script>	
		<script src="js/jquery.js"></script>
		<script src="js/ajaxValidator.js"></script>
	</body>
</html>
