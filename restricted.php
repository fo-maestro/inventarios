<?php
	session_start();
	if(isset($_SESSION['username'])){
		header("Location: index.php");
	}
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Restricted</title>
		<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
		<style type="text/css" media="screen">
			body {
			    padding-top: 90px;
			    background-color: rgb(0, 71, 128);
			}
			.panel-login {
				border-color: #ccc;
				-webkit-box-shadow: 0px 2px 3px 0px rgba(0,0,0,0.2);
				-moz-box-shadow: 0px 2px 3px 0px rgba(0,0,0,0.2);
				box-shadow: 0px 2px 3px 0px rgba(0,0,0,0.2);
			}
		</style>
	</head>
	<body>
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-md-offset-3">
					<div class="panel panel-login">
						<div class="page-header text-center">
							<h2>Acceso Denegado</h2>
						</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-lg-12 text-center">
									<p>Se le redireccionara a la Pagina de logueo en un instante...</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<script src="js/jquery.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				window.setTimeout('window.location.replace("login.php")', 3000);
			});
		</script>
	</body>
</html>